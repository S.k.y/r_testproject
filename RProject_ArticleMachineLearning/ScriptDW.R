#library(waveslim)
library(wavelets)
library(wmtsa)
library(MASS)
library(wavethresh)
library(dygraphs)

#-------------------- Prepare data -----------------------------#

dataFromFile = read.table("TestFolder\\2010_06_09_12_00_00_000.log", FALSE, ";")
dataFromFile1 = read.table("TestFolder\\2010_06_09_15_00_00_000.log", FALSE, ";")
dataFromFile2 = read.table("TestFolder\\2010_06_09_18_00_00_000.log", FALSE, ";")
dataFromFile3 = read.table("TestFolder\\2010_06_09_21_00_00_000.log", FALSE, ";")
dataFromFile4 = read.table("TestFolder\\2010_06_10_00_00_00_000.log", FALSE, ";")
dataFromFile5 = read.table("TestFolder\\2010_06_10_03_00_00_000.log", FALSE, ";")
dataFromFile6 = read.table("TestFolder\\2010_06_10_06_00_00_000.log", FALSE, ";")
dataFromFile7 = read.table("TestFolder\\2010_06_10_09_00_00_000.log", FALSE, ";")
tmpAllData = do.call(rbind, list(dataFromFile, dataFromFile1, dataFromFile2, dataFromFile3
                                          , dataFromFile4, dataFromFile5, dataFromFile6, dataFromFile7))

replaceDataFromFile = do.call(rbind, list(tmpAllData, tmpAllData, tmpAllData))
values = replaceDataFromFile[, 2]
#-------------------- Prepare data -----------------------------#
#plot.ts(values)


#--------------------- Charts with V and W values----------------#
valuesDWT <- dwt(values, n.levels = 8)
valuesDWT_V <- unlist(valuesDWT@V[valuesDWT@level])
valuesDWT_W <- unlist(valuesDWT@W[valuesDWT@level])
plot.ts(valuesDWT_V)
plot.ts(valuesDWT_W)
#--------------------- Charts with V and W values ----------------#


#-------------------------- Prediction from V values -------------#
timeSeries = ts(seq(values[1]), frequency = 12)

timeSeriesDWT = ts(valuesDWT_V, frequency = 12)
timeSeriesDWT_HW <- HoltWinters(timeSeriesDWT)
timeSeriesDWT_HW
predictionDWT_HW <- predict(timeSeriesDWT_HW, n.ahead = 12, prediction.interval = TRUE)
predictionDWT_HW

plot(timeSeriesHW)
plot(predictionHW)

resultDWT <- cbind(timeSeriesDWT, predictionDWT_HW)

#dygraph(resultDWT, "Forecast DWT (V parameter)") %>%
  #dySeries("timeSeriesDWT", label = "Actual") %>%
  #(c("predictionDWT_HW.lwr", "predictionDWT_HW.fit", "predictionDWT_HW.upr"), label = "Predicted")

#-------------------------- Prediction from V values -------------#



#-------------------------- Prediction from W values -------------#
timeSeriesDWT = ts(valuesDWT_W, frequency = 12)
timeSeriesDWT_HW <- HoltWinters(timeSeriesDWT)
timeSeriesDWT_HW
predictionDWT_HW <- predict(timeSeriesDWT_HW, n.ahead = 12, prediction.interval = TRUE)
predictionDWT_HW

#plot(timeSeriesDWT)
#plot(predictionDWT_HW)

resultDWT <- cbind(timeSeriesDWT, predictionDWT_HW)


#dygraph(resultDWT, "Forecast DWT (W parameter)") %>%
  #dySeries("timeSeriesDWT", label = "Actual") %>%
  #dySeries(c("predictionDWT_HW.lwr", "predictionDWT_HW.fit", "predictionDWT_HW.upr"), label = "Predicted")
#-------------------------- Prediction from W values -------------#


#timeSeriesDWT = ts(valuesDWT_W, frequency = 12)
#timeSeriesDWT_HW <- HoltWinters(timeSeriesDWT)
#timeSeriesDWT_HW
#predictionDWT_HW <- predict(timeSeriesDWT_HW, n.ahead = 12, prediction.interval = TRUE)
#predictionDWT_HW

#plot(timeSeriesDWT)
#plot(predictionDWT_HW)

#resultDWT <- cbind(timeSeriesDWT, predictionDWT_HW)

#dygraph(resultDWT, "Forecast DWT (W parameter)") %>%
  #dySeries("timeSeriesDWT", label = "Actual") %>%
  #dySeries(c("predictionDWT_HW.lwr", "predictionDWT_HW.fit", "predictionDWT_HW.upr"), label = "Predicted")




# ------------------------ Forecast without DTW ------------------------#

#timeSeries = ts(values, frequency = 12)
#timeSeries_HW <- HoltWinters(timeSeriesDWT)
#timeSeries_HW
#prediction_HW <- predict(timeSeries_HW, n.ahead = 12 * 8, prediction.interval = TRUE)
#prediction_HW

#plot(timeSeries)
#plot(prediction_HW)

#result <- cbind(timeSeries, prediction_HW)

#dygraph(result, "Forecast natural (without DWT)") %>%
  #dySeries("timeSeries", label = "Actual") %>%
  #dySeries(c("prediction_HW.lwr", "prediction_HW.fit", "prediction_HW.upr"), label = "Predicted")

