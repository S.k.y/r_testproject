library(forecast)
library(anytime)
library(rdatamarket)
library(ggplot2)
library(tseries)
library(TTR)
library(dygraphs)

#load data --------------------------
#dir = list.files("TestFolder", pattern = "*.log")
#dir
#dataSet = read.table("2010_06_09_12_00_00_000.log", FALSE, ";")
#for (file in dir){
  #if the merged dataset doesn't exist, create it
  #if (!exists("dataset")){
    #dataset <-  read.table(file.path(), FALSE, ";")
  #}
  
  #if the merged dataset does exist, append to it
  #if (exists("dataset")){
    #temp_dataset <- read.table(file, FALSE, ";")
  
    #dataset<-rbind(dataset, temp_dataset)
    #rm(temp_dataset)
  #}
#}
data1 = read.table("2010_06_09_12_00_00_000.log", FALSE, ";")
data2 = read.table("2010_06_09_15_00_00_000.log", FALSE, ";")
data3 = read.table("2010_06_09_18_00_00_000.log", FALSE, ";")
data4 = read.table("2010_06_09_21_00_00_000.log", FALSE, ";")
data5 = read.table("2010_06_10_00_00_00_000.log", FALSE, ";")
data6 = read.table("2010_06_10_03_00_00_000.log", FALSE, ";")
data7 = read.table("2010_06_10_06_00_00_000.log", FALSE, ";")
data8 = read.table("2010_06_10_09_00_00_000.log", FALSE, ";")
data9 = read.table("2010_06_10_12_00_00_000.log", FALSE, ";")
data10 = read.table("2010_06_10_18_00_00_000.log", FALSE, ";")
data11 = read.table("2010_06_10_21_00_00_000.log", FALSE, ";")
data12 = read.table("2010_06_11_00_00_00_000.log", FALSE, ";")
#load data --------------------------

#prepare data --------------------
alldata = data1;
#alldata = do.call(rbind, list(data1, data2, data3, data4, data5, data6, data7, data8, data9, data10, data11, data12))
#alldata
#values = alldata[, 2]
values = sample(seq(from = 20, to = 50, by = 5), size = 48, replace = TRUE)
times = anytime(alldata[, 1] / 1000, tz = "GMT")
alldata[,1] = anytime(alldata[, 1] / 1000, tz = "GMT") 
#prepare data --------------------

#work with data ------------------------
#12 - by months (4 - by quarters) start - start with 1946 year 1 quarter
timeSeriesData = ts(values, frequency = 12)
timeSeriesData
#logTimeSeriesData <- log(timeSeriesData) 

#Smoothing time series data.
#timeSeriesDataSMA <- SMA(timeSeriesData, n=3)
#timeSeriesDataSMA

#decompose return seasonal, trend, random values
#timeSeriesComponents$seasonal, timeSeriesComponents$trend, timeSeriesComponents$random
timeSeriesComponents <- decompose(timeSeriesData)
timeSeriesHW <- HoltWinters(timeSeriesData)
timeSeriesHW
predictionHW <- predict(timeSeriesHW, n.ahead = 12, prediction.interval = TRUE)
predictionHW
all <- cbind(timeSeriesData, predictionHW)
#work with data ------------------------

plot.ts(timeSeriesData)
#plot.ts(timeSeriesDataSMA)
plot(timeSeriesComponents)
plot(timeSeriesHW)
plot(predictionHW)

dygraph(all, "Forecast tension of the atmosphere (RU)") %>%
  dySeries("timeSeriesData", label = "Actual") %>%
  dySeries(c("predictionHW.lwr", "predictionHW.fit", "predictionHW.upr"), label = "Predicted")
